let run = require('./number-to-string.js');

let valid = [
    [0, '0'],
    [-23, '-23'],
    [-23.001, '-23.001'],
    [Infinity, 'Infinity'],
    [Number.NEGATIVE_INFINITY, '-Infinity'],
];

let invalid = [NaN, null, undefined, 'hi'];

test.each(valid)('%p is %p', (number, string) => {
    expect( run(number) ).toEqual(string);
})

test.each(invalid)('%p is undefined', (value) => {
    expect( run(value) ).toEqual(undefined);
})
