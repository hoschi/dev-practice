// challenge: https://www.codewars.com/kata/551b4501ac0447318f0009cd

let booleanToString = (b) => b ? "true" : "false";

module.exports = booleanToString;
