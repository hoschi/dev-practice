let run = require('./string-to-number.js');

let valid = [
    [ '0', 0 ],
    [ '42', 42 ],
    [ '-3.1415', -3.1415],
];

let invalid = [undefined, null, '', 'text', 'string123'];
let response_to_invalid = NaN;

test.each(valid)('%p becomes %p', (text, number) => {
    expect( run(text) ).toEqual(number);
});

test.each(invalid)(`%p becomes ${response_to_invalid}`, (input) => {
    expect( run(input) ).toEqual(response_to_invalid);
})
