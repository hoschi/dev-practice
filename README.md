# dev-practice

Practicing Software development for multiple languages and frameworks.

Here is the place to put all the coding and developing-exercises I will be doing
from this point on (2023-09-17).

## Why?

- documenting personal progress
- streamlining responsibility to continue practical learning
- putting some reference implementation out there

## General Structure

`<language or framework name>/<source or plattform of task>/`

## Plattforms

### [codewars.com](https://codewars.com)

![my badge][codewarsbadge] 
[profile][codewarsprofile]

- Directory structure:
  `codewars.com/<level type>/<level>/<name>--<hash from url>/`
- Directory [example](https://www.codewars.com/kata/53ee5429ba190077850011d4):
  `codewars.com/kyu/8/you-cant-code-under-pressure-n1--53ee5429ba190077850011d4/`

[codewarsbadge]: https://www.codewars.com/users/ac3a/badges/small
[codewarsprofile]: https://www.codewars.com/users/ac3a

### more possible sources

- https://codesignal.com/
- https://firecode.io/
- https://hackerrank.com/
- https://leetcode.com/
- https://topcoder.com/
