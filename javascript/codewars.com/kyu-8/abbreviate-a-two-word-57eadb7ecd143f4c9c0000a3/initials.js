// challenge: https://www.codewars.com/kata/57eadb7ecd143f4c9c0000a3

function abbrevName(name){
  let words = name.split(' ');
  let initials = words.map((word) => word[0].toUpperCase());
  return `${initials[0]}.${initials[1]}`;
}

module.exports = abbrevName
