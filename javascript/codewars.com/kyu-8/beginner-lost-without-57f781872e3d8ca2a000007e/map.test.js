let run = require('./map.js');

let cases = [
    // input, expected output
    [ [], [] ],
    [ [-5, 1, 32], [-10, 2, 64] ],
    [ [0.2], [0.4] ],
];

test.each(cases)("testing %p", (input, expected) => {
    const result = run(input);
    expect(result).toEqual(expected);
})
