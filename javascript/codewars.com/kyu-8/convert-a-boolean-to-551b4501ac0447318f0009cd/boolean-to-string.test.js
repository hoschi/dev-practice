let run = require('./boolean-to-string.js');

let cases = [
    [ undefined, false],
    [ null, false],
    [ "", false],
    [ "something", true],
    [ "0", true],
    ["false", true],
];

test.each(cases)("%p should be %p", (text, bool) => {
    expect( run(text) ).toEqual(bool.toString());
})
