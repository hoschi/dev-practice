let run = require('./initials.js');

let cases = [
    ["Maya Hummels", "M.H"],
    ["A B", "A.B"],
    ["areallylongname withaprettyannoyingsurname", "A.W"],
];

test.each(cases)("%p to %p", (longname, initials) => {
    let real = run(longname);
    expect(real).toEqual(initials);
});
