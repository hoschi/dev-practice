var run = require('./string-increment.js');

let appendable = [
    "f", "", String.empty,
    "3Ab", "hjg5a"
];

let increasable = [
    [ "3", "4" ],
    [ "9", "10" ],
    [ "009", "010" ],
    [ "n1", "n2" ],
    [ "75hi8", "75hi9" ],
];

test.each(appendable)('%p gets an appended 1', (input) => {
    expect( run(input) ).toEqual(input + "1");
});

test.each(increasable)('%p becomes %p', (input, result) => {
    expect( run(input) ).toEqual(result);
});

	// // sorting in-place because
	// // Array.prototype.toSorted() is not compatible with node v18
	// let keys = Object.keys(increasable).sort();
	// keys.forEach((key) => {
	// 	expectEquality(
	// 		increasable[key],
	// 		run(key)
	// 	);
	// });

