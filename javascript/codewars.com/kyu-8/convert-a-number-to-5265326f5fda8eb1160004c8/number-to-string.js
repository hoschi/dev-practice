// challenge: https://www.codewars.com/kata/5265326f5fda8eb1160004c8

function numberToString(num) {
  if( num === null || isNaN(num) )
    return undefined;

  return num.toString();
}

module.exports = numberToString;
