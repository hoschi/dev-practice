let run = require('./gigasecond.js');

let cases = [
	[
		new Date(2015, 1, 24, 22),
		new Date(2046, 10, 2, 23, 46, 40),
		1,
	],
];

test.each(cases)('%p until %p', (start, end, result) => {
	expect( run(start, end) ).toEqual(result);
});
