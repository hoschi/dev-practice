function initSieve(maximum) {
    let sieve = [];
    for(let n = 2; n <= maximum; n++ ) {
        sieve[n] = n;
    }
    return sieve;
}

export const primes = maximum => {

    if( maximum < 2)
        return [];

    let primeNums = initSieve(maximum);

    // iterate on a copy so we can modify the original array
    primeNums.slice().forEach( (factor1) => {
        if( !factor1 )
            return;

        primeNums.slice().forEach( (factor2) => {
            if (!factor2)
                return;

            let product = factor1 * factor2;
            if (product <= maximum)
                primeNums[product] = undefined;
        });
    });

    return primeNums.filter((p) => {
        return typeof p === 'number'
    });
};
