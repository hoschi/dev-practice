// task: https://www.codewars.com/kata/54a91a4883a7de5d7800009c

function incrementString (str) {
  if ( /\d$/.test(str)) {
    let digits = str.match(/\d*$/)[0];
    let number = Number.parseInt(digits);
    number++;
    let prefix = str.slice(0, -1 * digits.length);
    let suffix = number.toString().padStart(digits.length, '0');
    return prefix + suffix;
  }
  else {
    str += '1';
    return str;
  }
}

module.exports = incrementString;
