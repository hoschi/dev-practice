// challenge: https://exercism.org/tracks/javascript/exercises/gigasecond

const milli = 1000;
const giga = 1000000000;

function timespanInGigaseconds(startDate, endDate) {

	let milliseconds = endDate - startDate;
	return milliseconds * 1.0 / milli / giga; 
}

module.exports = timespanInGigaseconds;

