// challenge: https://www.codewars.com/kata/544675c6f971f7399a000e79

const stringToNumber = function(str){
  if( ! str )
    return NaN;
  return 1 * str;
}

module.exports = stringToNumber;
